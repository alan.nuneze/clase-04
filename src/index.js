const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.set('port',3000);

app.get('/', (req,res) => {
    res.send('<h1>CREANDO API REST</h1>');
});

app.get('/:num1/:num2', (req,res) => {
        const value1 = req.params.num1;
        const value2 = req.params.num2;
        res.json({
            result: sumValues(value1,value2)
        });
}); // asi se verá el path: /5/3 (ejemplo)

app.listen(app.get('port'), () => {
    console.log("Server on port 3000");
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.post('/', (req,res) => {
    if (req.body.num1 && req.body.num2){
        res.status(200).json({
            result: multiply(req.body.num1, req.body.num2)
        });
    }
    else{
        res.status(404).json({
            error: 'Something is missing'
        });
    }

});

function sumValues(num1,num2){
    return Number(num1) + Number(num2);
}

function multiply(num1,num2){
    return Number(num1)*Number(num2);
}